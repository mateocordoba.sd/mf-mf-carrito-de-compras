import React, { useState, useEffect, useRef } from 'react';
import { Button, Card, Col, Container, Row } from 'react-bootstrap';
import { useParams } from 'react-router-dom';


import { getProduct, currency, Product } from 'home/products';
import placeAddToCart from 'addToCart/placeAddToCart';

export default function PDPContent() {
    const {id} = useParams();
    const [product, setProduct]: [Product, React.Dispatch<any>] = useState({});

    useEffect(() => {
        if (id) {

            getProduct(id).then(setProduct)
        } else {
            setProduct({})
        }
    }, [id]);

    
    const addToCart = useRef(null);

    useEffect(()=>{
        if(addToCart.current){
            placeAddToCart(addToCart.current, product.id)
        }
    },[product])

    if (!product) return null;


    return (
        <Container fluid>

            <Row className="px-2">

                <Col xs={{ "span": 6, "offset": 3 }} md={{"span":4, "offset":4}} >
                    <Card key={product.id} >
                        <Card.Img variant="top" src={product.image} height={'300px'} />
                        <Card.Body>
                            <Card.Title>{product.name}</Card.Title>
                            <Card.Text>
                                {product.longDescription}
                            </Card.Text>
                            <div className="d-flex">

                                <Button variant="primary">{currency.format(product.price)}</Button>

                                <div ref={addToCart}></div>
                                
                            </div>
                        </Card.Body>
                    </Card>
                </Col>



            </Row>

        </Container>

    )






}