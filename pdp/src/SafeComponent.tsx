import React , {useState}from "react";

export default class SafeComponent extends React.Component<{}, {hasError:boolean}> {


    constructor(props:any){
        super(props);
        this.state = {hasError:false};
 
    };

    
    static getDerivedStateFromError(error:any){
        
        return {hasError:true}
    
    };  

    

    componentDidCatch() {
        
    };

    render(){
        if(this.state.hasError){
            
            return (<h1>Algo ha salido mal</h1>)

        }
        return this.props.children
        
    }
}

