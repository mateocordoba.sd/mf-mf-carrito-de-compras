import React, {Suspense, useEffect, useState}from "react";
import ReactDOM from "react-dom";

import "./index.css";
import "remixicon/fonts/remixicon.css";


//const Header = React.lazy( ()=> import('components/Header')); // Declaramos el header como un componente asincrono

import SafeComponent from "./SafeComponent";
import PDPContent from "./components/PDPContent";
import MainLayout from 'home/MainLayout';


ReactDOM.render(<MainLayout />, document.getElementById("app"));
