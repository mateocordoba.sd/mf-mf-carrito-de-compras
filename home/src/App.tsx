import React from "react";
import ReactDOM from "react-dom";

import "remixicon/fonts/remixicon.css";
import MainLayout from "./Main-layout";



ReactDOM.render(<MainLayout />, document.getElementById("app"));
