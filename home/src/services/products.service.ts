const API_SERVER = 'http://localhost:9002';


export const getProducts = () => {
    
    return fetch(`${API_SERVER}/products`).then(res => res.json())
}


export const getProduct = (productId:number)=> {
    
    return fetch(`${API_SERVER}/products/${productId}`).then(res => res.json())
}

export const currency = new Intl.NumberFormat("es-CO",{
    style: "currency",
    currency: "COP"
})