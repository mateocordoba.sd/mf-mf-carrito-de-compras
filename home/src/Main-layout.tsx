import React from "react";
// Router
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import Header from "./components/header/Header";
import Footer from "./components/footer/Footer";
import PDPContent from "pdp/PDPContent";
import HomeContent from "./components/home-content/HomeContent";
import CartContent from "cart/CartContent";


import './index.css';



export default function MainLayout() {
    return(


    <Router>
        <div className="container-fluid px-0">
            <Header />
            <div>
                <Routes>
                    <Route path="/" element={ <HomeContent /> }/>
                    <Route path="/product/:id" element={ <PDPContent /> }/>
                    <Route path="/cart" element={ <CartContent /> }/>

                </Routes>

                

            </div>

            



            <Footer />
        </div>

    </Router>
    )


}