import React, {FC} from "react";

import MiniCart from 'cart/MiniCart';


interface FooterProps {};

const Footer:FC<FooterProps> = () => {
    return (
        <div className="container-fluid px-0 bg-dark w100 fixed-bottom">
            <div className="container">

            </div>
            <MiniCart></MiniCart>
            <div className="row">
                <div className="col"> 
                    <h3 className="text-light">FOOTER HOME</h3>
                </div>
            </div>
        </div>
    );
};

export default Footer