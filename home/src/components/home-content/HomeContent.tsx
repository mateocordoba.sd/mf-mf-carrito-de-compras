import { useState, useEffect } from "react";
import { getProducts, getProduct } from "../../services/products.service";

import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { Col, Container, Row } from "react-bootstrap";


import {userLoggedIn, addToCart} from 'cart/cartService';
import { Link } from "react-router-dom";

export default function HomeContent() {


    const [products, setProducts] = useState([]);

    useEffect(() => {
        getProducts().then(setProducts)

    }, [])

    return (
        <Container fluid>
            
            <Row className="px-5">
                {products?.map((product:any) => (
                    <Col xs="12" sm="4" md="3" lg="3"  >
                    <Card  key={product.id} >
                        <Link to={`/product/${product.id}`}>

                        <Card.Img variant="top" src={product.image} />
                        </Link>
                        <Card.Body>
                            <Link to={`/product/${product.id}`}>

                            <Card.Title>{product.name}</Card.Title>
                            </Link>
                            <Card.Text>
                                {product.description}
                            </Card.Text>
                            <div className="d-flex">

                            <Button variant="primary">$ {product.price}</Button>
                            
                            <Button id={`add-to-cart-${product.id}`} variant="primary" onClick={()=> addToCart(product.id)}>Agregar al carrito</Button>
                            </div>
                        </Card.Body>
                    </Card>
                    </Col>

                ))}

            </Row>

        </Container>
    )
}