import React, {FC} from "react";

import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

import MiniCart from 'cart/MiniCart';
import Login from 'cart/Login';
import { Link } from "react-router-dom";


const Header:any = () => {
    return (
        <>
         <Navbar bg="dark" data-bs-theme="dark" style={{ marginBottom: "20px"}}>
        <Container fluid>
          <Navbar.Brand href="#home">Spinners</Navbar.Brand>
          <Nav className="justify-content-end">
            <Nav.Link ><Link to="/">Home</Link></Nav.Link>
            <Nav.Link ><Link to="/cart">Cart</Link></Nav.Link>
            <Nav.Link ><MiniCart /></Nav.Link>
            <Nav.Link> <Login /></Nav.Link>
           
          </Nav>
        </Container>
      </Navbar> 

      
        </>
        
    );
};

export default Header