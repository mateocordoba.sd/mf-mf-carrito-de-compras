import React, {useState, useEffect} from 'react';

import {cart, clearCart} from '../services/cart.service';

import {currency} from 'home/products';

export default function MiniCart(){
   

    const [items, setItems] = useState([]);
    const [showCart, setShowCart] = useState(false);

    useEffect(():any =>{
        const value:any = cart.value
        setItems(value?.cartItems);
        return cart.subscribe((c:any)=>{
            setItems(c?.cartItems)
        })

    },[])

    if(!items) return null

    return(

        <>
            <span onClick={()=> setShowCart(!showCart)}>
                <i className="ri-shopping-cart-2-fill text-2xl" id="showcart">
                    {items.length}
                </i>

            </span>
            {showCart && (
                <>
                <div 
                    className="absolute p-2 border-4 border-blue-800"
                    style={{
                        width: 400,
                        top: 50,
                        position: "absolute",
                        right: 0,
                        zIndex: 1,
                        backgroundColor:"#3b3b3b"
                    }}
                >
                    <div className=" text-sm"
                        style={{
                            gridTemplateColumns:"1fr 3fr, 10fr, 2fr"
                        }}
                        >
                            {items.map((item:any)=>(
                                <div className='d-flex justify-content-evenly my-2' key={item.id}>
                                    <div>{item.quantity}</div>
                                    <img src={item.image} alt={item.name} style={{width:30, height:30}}/>
                                    <div>{item.name}</div>
                                    <div className='text-right'>{currency.format(item.quantity * item.price)}</div>

                                </div>

                            ))}


                    </div>
                    <div className='text-end'> Total:  </div>
                    <div className='d-flex justify-content-end mt-4 vaciar-pagar-buttons-div'>

                        <button onClick={clearCart} className='mt-3 btn btn-outline-danger'>
                            vaciar carrito
                            

                        </button>
                        <button onClick={clearCart} className='mt-3 btn btn-outline-success'>
                            Pagar
                            

                        </button>
                    </div>
                </div>
                
                </>
            )}
        </>



    )

    

}