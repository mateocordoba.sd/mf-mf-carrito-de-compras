import React, { useState, useEffect } from "react";


import {
  
  clearCart,
  cart,
} from "../services/cart.service";

import {currency} from 'home/products';


export default function CartContent() {

  const [items, setItems] = useState([]);



  useEffect(
    (): any => cart.subscribe((value: any) => setItems(value?.cartItems ?? [])),
    []

  );

  if (items.length === 0) return null;

  return (
    


      <div className="container-fluid  center">
        <div className="row">

          {items.map((item: any) => (
            <div className="d-flex justify-content-evenly my-2" key={item.id}>
              <div>{item.quantity}</div>
              <img
                src={item.image}
                alt={item.name}
                style={{ width: 50, height: 50 }}
              />
              <p>{item.name}</p>
              <p className="text-right">{currency.format(item.quantity * item.price)}</p>
            </div>
          ))}
        </div>
        <div className="row mx-4">

          <div className='text-end'> Total:  </div>
          <div className='d-flex justify-content-end mt-4 vaciar-pagar-buttons-div '>

            <button onClick={clearCart} className='mt-3 btn btn-outline-danger'>
              vaciar carrito


            </button>
            <button onClick={clearCart} className='mt-3 btn btn-outline-success'>
              Pagar


            </button>
          </div>
        </div>
      </div>
  
  );
}
