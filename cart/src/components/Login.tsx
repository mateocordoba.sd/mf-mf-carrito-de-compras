import React, {useState} from 'react'

import {login, userLoggedIn} from '../services/cart.service'

export default function Login(){
    const loggedIn = userLoggedIn();
    const [showLogin, setShowLogin] = useState(false);

    const [username, setUsername] = useState("Zoro");
    const [password, setPasword] = useState("123");

    
    

    if(loggedIn) return null;

    return(
        <>
        <span onClick={()=> setShowLogin(!showLogin)}>
            <i className="ri-fingerprint-line text-2x1 text-light " id='showLogin' ></i>
        </span>
        {showLogin && (
            <div className="absolute p-2 border-4 border-blue-800 text-light" 
            style={{
                width: 350,
                top: 50,
                position: "absolute",
                right: 0,
                zIndex: 1,
                backgroundColor:"#3b3b3b"
            }}
            >
                <input 
                    type="text"
                    placeholder="Numbre de usuario"
                    value={username}
                    onChange={(evt) => setUsername(evt.target.value)}
                    className='border text-sm border-gray-400 p-2 rounded-md w-full' />

                <input 
                    type="text"
                    placeholder="Contraseña"
                    value={password}
                    onChange={(evt) => setPasword(evt.target.value)}
                    className='border text-sm border-gray-400 p-2 rounded-md w-full' />

                <button 
                    className='bg-green-900 text-white py-2 px-5 rounded-md'
                    onClick={()=> login(username, password)}
                    id='loginBtn'>
                    Login
                </button>
            </div>
        )}
        </>

    ); 
    
}

