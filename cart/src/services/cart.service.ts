import { useState, useEffect } from 'react';
import {BehaviorSubject} from 'rxjs';

export const API_SERVER = 'http://localhost:9002';

export const jwt = new BehaviorSubject(null);
export const cart = new BehaviorSubject(null)


export const login = (username:string, password:string) => 
    fetch(`${API_SERVER}/auth/login`,{
        method:"POST",
        headers:{
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            username,
            password
        }),

    })
    .then(res => res.json())
    .then((data:any) => {
        jwt.next(data.access_token);
        console.log('====================================');
        console.log({...data});
        console.log('====================================');

        getCart();

        return data.access_token
    }) 

export const userLoggedIn =() => {
    const [loggedIn, setLoggedIn] = useState(!!jwt.value);
    useEffect(():any=>{
        setLoggedIn(!!jwt.value);
        return jwt.subscribe((c:any)=>{
            setLoggedIn(!!jwt.value)
        });
    }, [])
    return loggedIn
}



export const getCart = ()=>{ 
    fetch(`${API_SERVER}/cart`,{
        headers:{
            "Content-Type":"application/json",
            Authorization: `Bearer ${jwt.value}`
        },
        
    })
    .then((res:any)=>res.json())
    .then((res:any)=> {
        console.log('====================================');
        console.log(res);
        console.log('====================================');
       cart.next(res)
        
    })

}

export const addToCart = (id:any)=>{ // el id hace referencia al id del producto
    fetch(`${API_SERVER}/cart`,{
        method:"POST",
        headers:{
            "Content-Type":"application/json",
            Authorization: `Bearer ${jwt.value}`
        },
        body:JSON.stringify({id})
        
    }) 
    .then((res:any)=>res.json())
    .then(()=> {
        getCart()
    })

}


export const clearCart = (id:any)=>{
    fetch(`${API_SERVER}/cart`,{
        method: "DELETE",
        headers:{
            "Content-Type": "application/json",
            Authorization: `Bearer ${jwt.value}`
        }
    })
    .then((res:any)=> res.json())
    .then(()=>{
        getCart()
    })
}