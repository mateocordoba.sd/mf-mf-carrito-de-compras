import React from "react";
import ReactDOM from "react-dom";

import "remixicon/fonts/remixicon.css";
import "./index.css";

import Header from 'home/Header';
import Footer from 'home/Footer';
import CartContent from "./components/CartContent";
import MainLayout from 'home/MainLayout';



ReactDOM.render(<MainLayout />, document.getElementById("app"));
